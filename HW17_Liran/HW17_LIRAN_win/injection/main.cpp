#include "stdafx.h"

DWORD MyGetProcessId(LPCTSTR ProcessName) // non-conflicting function name
{
	PROCESSENTRY32 pt;
	HANDLE hsnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	pt.dwSize = sizeof(PROCESSENTRY32);
	if (Process32First(hsnap, &pt)) { // must call this first
		do {
			if (!lstrcmpi(pt.szExeFile, ProcessName)) {
				CloseHandle(hsnap);
				return pt.th32ProcessID;
			}
		} while (Process32Next(hsnap, &pt));
	}
	CloseHandle(hsnap); // close handle on failure
	return 0;
}

int main()
{

	//DWORD ID = GetProcessId("notepad.exe");
	char dll[MAX_PATH] = "";
	HANDLE proc;

	// Get full path of DLL to inject
	DWORD pathLen = GetFullPathNameA("newDll.dll", MAX_PATH, dll, NULL);

	// Get LoadLibrary function address �
	// the address doesn't change at remote process
	PVOID addrLoadLibrary = (PVOID)GetProcAddress(GetModuleHandleW(L"kernel32.dll"), "LoadLibraryA");

	while (TRUE)
	{
		DWORD ID = MyGetProcessId(TEXT("notepad.exe"));
		if (ID == 0)
		{
			std::cout << "ERROR" << std::endl;
		}
		else 
		{
			std::cout << ID << std::endl;

			// Open remote process
			proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, ID);
			std::cout << GetLastError() << std::endl;

			// Get a pointer to memory location in remote process,
			// big enough to store DLL path
			PVOID memAddr = (PVOID)VirtualAllocEx(proc, NULL, pathLen + 1, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
			if (NULL == memAddr) {
				int err = GetLastError();
				return 0;
			}

			// Write DLL name to remote process memory
			int check = WriteProcessMemory(proc, memAddr, dll, pathLen + 1, NULL);
			if (0 == check) {
				int err = GetLastError();
				return 0;
			}

			// Open remote thread, while executing LoadLibrary
			// with parameter DLL name, will trigger DLLMain
			HANDLE hRemote = CreateRemoteThread(proc, NULL, 0, (LPTHREAD_START_ROUTINE)addrLoadLibrary, memAddr, NULL, NULL);
			if (NULL == hRemote) {
				int err = GetLastError();
				return 0;
			}

			WaitForSingleObject(hRemote, INFINITE);
			check = CloseHandle(hRemote);
		}
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
	return 0;
}