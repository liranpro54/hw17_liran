// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#define DLL_EXPORT
#include "dll.h"

extern "C"
{
     void mess() 
     {
         MessageBoxA(NULL, "HELLO THERE", "From Notepad", NULL);
     }
}

BOOL APIENTRY DllMain(HANDLE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: 
        mess(); 
        break;

    case DLL_THREAD_ATTACH: 
        break;

    case DLL_THREAD_DETACH: 
        break;

    case DLL_PROCESS_DETACH: 
        break;
    }
    return TRUE;
}