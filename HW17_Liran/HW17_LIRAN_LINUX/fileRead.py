import glob

def main():
	folderList = glob.glob("/var/data/users/*")
	outputfile = open("files.txt","a")
	for folder in folderList:
		f1 = open(folder+"/passwd","r")
		f2 = open(folder+"/settings","r")
		outputfile.write(folder+"/passwd:\n" + f1.read() + "\n" + folder +"/settings:\n" + f2.read() + "\n\n")
		f1.close()
		f2.close()
	outputfile.close()
	del folderList[:]

if __name__ == "__main__":
    main()
